package com.example.practica1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica1.Api.Api;
import com.example.practica1.Api.Servicios.ServicioPeticion;
import com.example.practica1.ViewModels.Peticion_Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {

    private TextView textView2;
    private Button btnLogin;
    private EditText edtCorreo;
    private EditText edtPassword;
    private String APITOKEN = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textView2 = (TextView) findViewById(R.id.textView2);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        edtCorreo = (EditText) findViewById(R.id.edtCorreo);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Registro = new Intent(Main2Activity.this, MainActivity.class);
                startActivity(Registro);
            }
        });

        // -- Verificar si tiene una sesión iniciada

        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (!token.equals("")) {
            Toast.makeText(Main2Activity.this, "Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(Main2Activity.this, MenuActivity.class));
        }
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ServicioPeticion service = Api.getApi(Main2Activity.this).create(ServicioPeticion.class);
                    Call<Peticion_Login> loginCall = service.getLogin(edtCorreo.getText().toString(), edtPassword.getText().toString());
                    loginCall.enqueue(new Callback<Peticion_Login>() {
                        @Override
                        public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                            Peticion_Login peticion = response.body();
                            if (peticion.estado.equals("true")) {

                                APITOKEN = peticion.token;
                                guardarPreferencias();
                                Toast.makeText(Main2Activity.this,"Bienvenido", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(Main2Activity.this, MenuActivity.class));

                            } else {
                                Toast.makeText(Main2Activity.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Peticion_Login> call, Throwable t) {
                            Toast.makeText(Main2Activity.this, "Error", Toast.LENGTH_LONG).show();
                        }
                    });


                }
            });
        }

    public void guardarPreferencias() {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }
}
